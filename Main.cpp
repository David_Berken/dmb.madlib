#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>

using namespace std;

int main()
{
	char save = 'n';

	const int size = 12;
	string answers[size] =
	{
		"Enter an adjective(describing word) : ",
		"Enter a sport : ",
		"Enter a city : ",
		"Enter a person : ",
		"Enter an action verb(past tense) : ",
		"Enter a vehicle : ",
		"Enter a place : ",
		"Enter a noun : ",
		"Enter an adjective(describing word) : ",
		"Enter a food(plural) : ",
		"Enter a liquid : ",
		"Enter an adjective(describing word) : "
	};

	for (int i = 0; i < size; i++)
	{
		cout << answers[i];
		getline(cin, answers[i]);
	}

	string finalanswer = "One day my " + answers[0] + " friend and I decided to go to the " + answers[1] + " game in " + answers[2] + ".\n"
		+ "We really wanted to see " + answers[3] + " play.\n" + "So we " + answers[4] + " in the " + answers[5] + " and headed down to " + answers[6] + " and bough some " + answers[7] + ".\n"
		+ "We watched the game and it was " + answers[8] + ".\n" + "We ate some " + answers[9] + " and drank some " + answers[10] + ".\n"
		+ "We had a " + answers[11] + " time, and can't wait to go again.\n\n";

	cout << "\n" << finalanswer;
			
		cout << "Would you like to save output to file? (y/n): ";
		cin >> save;
	
		if (save == 'y' || save == 'Y')
		{
			string filepath = "MadLib.txt";
			ofstream ofs(filepath);
			ofs << finalanswer;
			ofs.close();
			cout << "\n" << "Mad lib has been saved to MadLib.txt.";
		}

		cout << "\n" << "Press any key to exit...";
		(void)_getch();
		return 0;
}